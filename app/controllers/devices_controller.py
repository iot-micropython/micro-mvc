from libraries.render import Render

import sys
from machine import UART

class Devices:

    def read(self):
        render = Render()
        uart = UART(1, 9600)
        uart.init(9600, bits=8, parity=None, stop=1, rx=22,  tx=23)
        while True:
            bar_code = uart.read(21)
            if bar_code is not None:
                return render.to_json( { 'code': bar_code } )


