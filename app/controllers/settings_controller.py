from config.environment import get_setting_file

from libraries.render import Render
from libraries.tools import if_file_exists 
from libraries.tools import get_setting
from libraries.tools import to_value 
from libraries.wifi import wifi

import btree

class Settings:

    def index(self):
        render = Render()
        render.add_style('settings.css')
        render.add_script('settings.js')

        try:
            if if_file_exists(get_setting_file()):
                data = get_setting(get_setting_file())

                render.add_variable('hostname', to_value(data['hostname']))
                if data['dhcp_client'].decode() == 'on':
                    render.add_variable('checked', 'CHECKED')
                else:
                    render.add_variable('checked', '')
                    render.add_variable('gateway', to_value(data['gateway']))
                    render.add_variable('ip', to_value(data['ip']))
                    render.add_variable('subnet_mask', to_value(data['subnet_mask']))
                    render.add_variable('dns', to_value(data['dns']))
                    render.add_variable('dns_2', to_value(data['dns_2']))

                render.add_variable('ssid', to_value(data['ssid']))
                render.add_variable('key', to_value(data['key']))
                render.add_variable('authmode', to_value(data['authmode']))
                render.add_variable('channel', to_value(data['channel']))

                render.add_variable('password', to_value(data['password']))
                render.add_variable('password_second', to_value(data['password_2']))
            else:
                render.add_variable('checked', 'CHECKED')

        except Exception as err:
            print("Error. {%s}" % err)

        return render.to_html('settings', 'index') 

    def save(self):
        render = Render()
        try:
            configuration = self.httpClient.ReadRequestPostedFormData()
        except Exception as err:
            print("Post Variables error. {%s}" % err)

        settings = open(get_setting_file(), "w+")
        db = btree.open(settings)
        try:
            for key, values in configuration.items():
                print(key, values)
                db[key] = values
                db.flush()
        except Exception as err:
            print("Save error. {%s}" % err)
        db.close()
        settings.close()
            
        return render.to_html('settings', 'save') 

    def wifi(self):
        render = Render()
        return render.to_json( wifi() )

