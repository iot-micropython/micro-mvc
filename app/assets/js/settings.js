/* ***
 * Javascript
 * @date 18 Oct
 * @year 2018
 */
$(document).ready(function(){
  if($('#dhcp').prop("checked") == true) {
    $("#static_configuration").hide();
  } else {
    $("#static_configuration").show();
  }
  $.ajax({
      url: "/settings/wifi.json",
      success: function(data){
          $.each(data.networks, function(key, el){
              if (el.selected == 'True') { 
                $("#ssid").append($("<option />").val(el.name).text(el.name).attr('selected', 'selected'));
                $("#channel").val(el.channel).attr('selected', 'selected');
                $("#authmode").val(el.auth_mode).attr('selected', 'selected');                
              } else {
                $("#ssid").append($("<option />").val(el.name).text(el.name));
              }
          });

          $("#ssid").change(function(){
              var currentNetworkName = $(this).val();
              data.networks.forEach(function(el){
                  if(el.name == currentNetworkName){
                       $("#channel").val(el.channel);
                       $("#authmode").val(el.auth_mode);
                   }
               });

          });
      },
      async: false
  });
});

$('#dhcp').click(function(){
  if($(this).prop("checked") == true) {
    $("#static_configuration").hide();
  } else {
    $("#static_configuration").show();
  }
});

$('#key_chk').click(function(){
  var key = document.getElementById("key");
  if($(this).prop("checked") == true) {
    key.type = "text";
  } else {
    key.type = "password";
  }
});

$('#admin_chk').click(function(){
  var password = document.getElementById("password");
  var password_2 = document.getElementById("password_2");
  if($(this).prop("checked") == true) {
    password.type = "text";
    password_2.type = "text";
  } else {
    password.type = "password";
    password_2.type = "password";
  }
});

