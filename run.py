"""
This is a code by use the code to bar code reader
01 October 2018
05 October 2018
"""

from libraries.connect import connect

# connection information data
d = connect()
current_ip = d[0]
net_mask = d[1]
gateway = d[2]
dns = d[3]

print(current_ip)
print(net_mask)
print(gateway)
print(dns)

from libraries.handlers import handler
from libraries.web_srv import srv

srv(current_ip)

print("Start Server")
