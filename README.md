# BAR CODE Micropython

![implement the code for the bar code device](doc/code-29-11-2019.png "micropython code")



Update Micro MVC from Loboris to Micropython (vanilla version):

esptool.py --chip esp32 --port /dev/ttyUSB0 erase_flash

esptool.py --chip esp32 --port /dev/ttyUSB0 --baud 460800 write_flash -z 0x1000 esp32-ppp-fix.bin


Status:
=======
0. Read number from barcode device (done is master branch)
1. webinterface (branch: web-interface-01)
2. display



![module_hardware](images/lector-de-codigo-de-barras-sin-cables.jpg "the module")

01 October Mon, 15:10:22 2018
=============================


05 October Mon, 15:22:33 2018
=============================
![code sample](doc/sample-code.png "code sample provided by the manufacturer of board")


> Modulo Original para arduino lector de codigo de barras serial TTL, lo conectas como en la foto 1 copia y pega este codigo en el proyecto arduino y ves los codigos por monitor serie. Ademas el modulo es 100% configurable para usos multiples, lector fijo, lector por pulsador, tecla de confirmacion, indicador sonoro de lectura correcta. Arma ya tu proyecto facil y rapido.

> *NOTA:* El algunas versiones del modulo es necesario unir el pin 4 ( contado del lado del cable rojo) a masa (pin 8) de lo contrario emitirá un sonido continuo al recibir alimentación, quedaría 1 rojo 5v, 2 blanco TX, 4 y 8 blancos a masa GND. Vienen sin cables, pero funcionan óptimamente.

> Se retira por tribunales de lunes a viernes de 10 a 18hs.

> Codigo de ejemplo para su prueba:

> //Ejemplo modulo QS25 ITEA S.A. Funciona tambien con cualquier tipo de microprocesador ej PIC ESP8266
> //Conectar el cable de datos del modulo (pin 2 al lado del rojo) al pin 10 del arduino, cargar el programa en el arduino y abrir el monitor serie para comprobar su funcionamiento.
> //En caso de conectar a otro pin modificar la linea Softwareserial.
> //Recuerde que si hace un beep continuo, hacer un puente entre pin 8 (el mas alejado del rojo) y pin 4.

[Manufaturer board](http://iteasa.com.ar/index.php/es)

# Today my idea is proceed using this library to comunication between esp32 and QS25

[UART for Micropython](http://docs.micropython.org/en/v1.9.3/pyboard/library/pyb.UART.html)

Thu 14 Feb 2019 12:04:26 PM -03
=============================
![Reference](images/IMG_20190214_091237555.jpg "IMG (01)")

## Data Diagram

![Diagram of Data](doc/ClassDiagram.png "IMG (02)")

## Use Case

![Diagram of Data](doc/UseCaseDiagram.png "IMG (02)")

