from libraries.tools import capitalize

element = 'welcome'
module_name = "app.controllers." + element + "_controller"
class_name = capitalize(element)

app = __import__(module_name)
try:
    controller = getattr(eval(module_name), class_name)
except:
    print("Error open")
    print(module_name)
    print(class_name)
    print("Error close")

i = controller()
i.index()

