CCM1641CSLS2
ST100919R000701

Args:
rs (int):  Register select address GPIO pin.
en (int):  Enable GPIO pin.
d4 (int): Data4 GPIO pin.
d5 (int): Data5 GPIO pin.
d6 (int): Data6 GPIO pin.
d7 (int): Data7 GPIO pin.
cols (int): Number of character columns.
lines (int): Number of display rows.


lcd = CharLCD(22, 23, 5, 18, 19, 21, 16, 4)
