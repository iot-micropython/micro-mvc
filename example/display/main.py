from machine import Pin, SPI
import st7789py as st7789

spi = SPI(2, polarity=1, sck=Pin(19), mosi=Pin(23))
display = st7789.ST7789(spi, 240, 240, reset=Pin(18, Pin.OUT), dc=Pin(22, Pin.OUT))
display.init()


display.fill_rect(0, 0, 240, 50, st7789.color565(211, 79, 79))
display.fill_rect(0, 50, 240, 50, st7789.color565(224, 109, 56))
display.fill_rect(0, 100, 240, 50, st7789.color565(229, 179, 0))
display.fill_rect(0, 150, 240, 50, st7789.color565(0, 62, 91))
display.fill_rect(0, 200, 240, 40, st7789.color565(1, 46, 63))

from pytext import text as t
import pyfonts.romans

t(display, pyfonts.romans, "Marlboro", row=25, column=10, color=st7789.color565(250, 250, 250))
t(display, pyfonts.romans, "20 Unidades", row=75, column=10, color=st7789.color565(250, 250, 250))
t(display, pyfonts.romans, "", row=125, column=10, color=st7789.color565(250, 250, 250))
t(display, pyfonts.romans, "", row=175, column=10, color=st7789.color565(250, 250, 250))

current_color = st7789.color565(250, 250, 250)

while True:
    if current_color == st7789.color565(250, 250, 250):
        current_color = st7789.color565(250, 0, 0)
        t(display, pyfonts.romans, "${:>12}".format("75.90"), row=175, column=10, color=current_color)
    else:
        current_color = st7789.color565(250, 250, 250)
        t(display, pyfonts.romans, "${:>12}".format("75.90"), row=175, column=10, color=current_color)

