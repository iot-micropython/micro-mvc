import os
import glob

def clean(line):
    line = line.replace("\n", "")
    line = line.replace("\"", "")
    line = line.replace("\'", "")
    return line

for f in glob.glob("*.csv"):
    products_file = open(f)
    products = products_file.readlines()

    index = 0
    products[index] = clean(products[index])
    first = products[index].split("\t")
    index = len(products)-1
    products[index] = clean(products[index])
    last = products[index].split("\t")

    products_file.close()

    resume = open("segments.csv", "a+")

    name = "{}-{}.csv".format(first[0], last[0])
    row = "{}, {}\n".format(first[0], last[0])
    resume.write(row)
    
    resume.close()

    if(os.path.isfile(name)):
        print("exists", name)
    else:
        print("Error ", name)
        os.rename(f, name)

    


