import os
from machine import SDCard as SD

card = SD(slot=2, mosi=23, miso=19, sck=5, cs=18)
mount_point = "depot"
os.mount(card, "/{}".format(mount_point))

print('Start ...')
import btree
to = open('depot/database.db', "w+b")
db = btree.open(to)
os.listdir('depot')

import time
start = time.time()
with open('depot/verificador.csv', 'r') as csv:
    for line in csv:
        line = line.rstrip("\r\n")
        line = line.replace("'", "")
        line_list = line.split("\t")
        db[line_list[0].encode()] = line
    db.flush()

end = time.time()

total = end-start
print("start={} end={} Total={}".format(str(start), str(end), str(total)))

to.close()
db.close()
print('End ...')

