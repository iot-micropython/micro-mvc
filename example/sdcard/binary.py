n = [x ** 5 for x in range(1000)]

x = 21975035404576

start = 0
end = len(n)

while start < end:
    index = int((end-start) / 2) + start
    print([start, end], " <-> ", index, "x:{}, position: {}".format(x, n[index]))

    if n[index] == x:
        print(index, "~> ", n[index])
        break

    if n[index] > x:
        start = start
        end = index
    else:
        start = index
        end = end

