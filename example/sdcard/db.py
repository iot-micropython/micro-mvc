import os
from machine import SDCard as SD

card = SD(slot=2, mosi=23, miso=19, sck=5, cs=18)
mount_point = "depot"
os.mount(card, "/{}".format(mount_point))

import btree
to = open('depot/database.db', "w+b")
db = btree.open(to)
os.listdir('depot')

with open('depot/verificador.csv', 'r') as csv:
    lines = csv.read(1024*20).split("\n")
    
    while len(lines) > 0:
        for line in lines:
            line = line.replace("'", "")
            line_split = line.split("\t")
            print("> ", line_split[0], end=" ")
            print('.', end="")
            db[line_split[0].encode()] = line

        lines = csv.read(1024*20).split("\n")
    db.flush()

to.close()
db.close()
