#!/bin/bash

echo "cleanning" $1
for i in $( ampy --port /dev/ttyUSB0 ls $1 ); do
  ampy --port /dev/ttyUSB0 rm $i
done
ampy --port /dev/ttyUSB0 rmdir $1
echo "complete"
