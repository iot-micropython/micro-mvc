"""
2019 PyCamp
Configuration general

environment [ development, production, test]
welcome_page
templates_path
"""

environment = 'development'
welcome_page = 'welcome/index.html'
templates_path = '/app/assets'

def get_setting_file():
    return 'config/settings.db'

