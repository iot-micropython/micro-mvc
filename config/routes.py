"""
Routes - set urls
"""
from libraries.handlers import handler

routeHandlers = [
        ("welcome/index.html", "GET", handler),

        ("settings/index.html", "GET", handler),
        ("settings/save.html", "POST", handler),

        ("about/index.html", "GET", handler),
        ("contact.html", "GET", handler),

        ("settings/wifi.json", "GET", handler),
        ("devices/read.json", "GET", handler)
]
