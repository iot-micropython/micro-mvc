from libraries.tools import if_file_exists
from json import loads

class Configuration:
    _data = 'configuration.json'

    '''
    This function return an exception when the configuration file
    not exist.
    And return the configuration file when this exist.
    '''
    def get(self):
        if if_file_exists(_data):
            configuration_file = open(self._data)
            content = ''.join(configuration_file.readlines())
            json = loads(content)
            return json
        else:
            raise Exception('The congiguration file doesn\'t exist yet.')




