from microWebSrv import MicroWebSrv

from libraries.tools import capitalize
import app.controllers

def switch_type(ext):
    switcher = {"html": 1, "htm": 1, "json": 2 }
    return switcher.get(ext, False)

def handler(httpClient, httpResponse, routeArgs=None):
    path = httpClient.GetRequestPath()
    route  = path.split("/")
    component = route[1]
    action = route[2].split(".")
    obj = getattr(app.controllers, component + "_controller")
    controller = getattr(obj, capitalize(component))
    instance = controller()
    instance.httpClient = httpClient
    instance.httpResponse = httpResponse
    instance.routeArgs = routeArgs
    print(component)
    print(action)
    try:
        layout = getattr(instance, action[0])()
    except Exception as err:
        print("Handler {%s}" % err)

    #Check extension and returns a json response if needed
    if switch_type(action[1]) == 2:
        httpResponse.WriteResponseJSONOk( layout )

    if switch_type(action[1])==1:
        httpResponse.WriteResponseOk(
            headers = None,
            contentType    = "text/html",
            contentCharset = "UTF-8",
            content = layout)

