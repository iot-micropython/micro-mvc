"""
05  December 2018
This module launche the connection of the device to wifi
or AP mode for configuration
"""
from config.environment import get_setting_file

from time import sleep

from network import WLAN
from network import STA_IF
from network import AP_IF

from libraries.tools import if_file_exists 
from libraries.tools import get_setting

from libraries.configuration import Configuration
connection = []

def connect():
    return client_demo()

    """ main ask if the device have configuration saved or launch ap mode"""
    try:
        if if_file_exists(get_setting_file()):
            data = get_setting(get_setting_file())

        ssid = data["ssid"]
        key = data["key"]
        authmode = data["authmode"]
        channel = data["channel"]

        print(data)

        print("Client Mode ...")
        return client_mode(ssid, key) 
    except Exception as err:
        # return ap_mode()
        return client_demo()


def client_demo():
    sta = WLAN(STA_IF)
    sta.active(True)
    nets = sta.scan()

    sta.connect("<ssid>", "<password>")

    while not sta.isconnected():
        sleep(2)
        connection = sta.ifconfig()
    return connection

def client_mode(ssid, key):
    """client_mode connect the device to the network in configuration file"""
    sta = WLAN(STA_IF)
    sta.active(True)
    nets = sta.scan()
    sta.connect(ssid, key)
    while not sta.isconnected():
        sleep(2)
        connection = sta.ifconfig()

    del sta
    return connection

def ap_mode():
    """ap_mode is a method with start the device in AP mode allow new configuration"""
    sta = WLAN(STA_IF)
    ap = WLAN(AP_IF)
    sta.active(True)
    ap.active(True)
    ap.config(essid='Bar_Code_IOT')
    print(ap.ifconfig())
    connection = sta.ifconfig()
    return connection
 
if __name__ == "__main__":
    main()


