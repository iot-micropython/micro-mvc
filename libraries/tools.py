from os import stat
from json import load

import btree

def if_file_exists(file_name):
    try:
        stat(file_name)
        return True
    except:
        return False

def get_setting(file_name):
    try:
        settings = open(file_name)
        data = btree.open(settings)
    except Exception as err:
        msg = "Error [tools -> getsettings] {%s}" % err
        print(msg)
        return False
    return data

def to_value(text):
    return " value=\"{}\" ".format(text.decode())

def capitalize(string):
    return string[0].upper()+string[1:]
