class Render:
    def __init__(self): 
        self._var = ()
        self._styles = (("pure-min.css"),("side-menu.css"),("styles.css"),)
        self._scripts = (("jquery-3.4.1.min.js"),)

    def to_html(self, component, filename):
        self._component = component
        self._filename = filename
        combine = self.combine()
        return combine

    def to_json(self, json):
        return json

    def add_style(self, filename):
        self._styles = self._styles + ((filename),)

    def add_script(self, filename):
        self._scripts = self._scripts + ((filename),)

    def add_variable(self, name, content):
        self._var = self._var + ((name, content), )

    def read_content(self, filename, component=None):
        if component == None:
            full_file_path = "/app/views/" + filename + ".html"
        else:
            full_file_path = "/app/views/"+ component + "/" + filename + ".html"
        try:
            template=open(full_file_path, 'r')
            html=template.readlines()
            template.close()
            del template
            return ''.join(html)
        except:
            return "Error Open: " + filename

    def combine(self):
        layout = self.read_content('layout')
        navigation = self.read_content('navigation')

        page = self.read_content(self._filename, self._component)
        page = self.merge_variables(page)

        layout = layout.replace("{navigation}", navigation)
        del navigation
        layout = layout.replace("{main-content}", page)
        del page

        layout = self.update_libs(layout)
        return layout

    def merge_variables(self, layout):
        for field in self._var:
            flag = "{%s}" % field[0]
            try:
                layout = layout.replace(flag, field[1])
                del field
            except Exception as err:
                print("merge_variables error. {%s}" % err)
        del self._var
        return layout

    def update_libs(self, layout):
        tag_script = ""
        tag_style = ""
        for script_file in self._scripts:
            tag_script = tag_script + "<script type=\"text/javascript\" src=\"/js/" + script_file + "\" charset=\"utf-8\"></script>"
            del script_file

        for style_file in self._styles:
            tag_style = tag_style + "<link href=\"/css/" + style_file + "\" rel=\"stylesheet\" type=\"text/css\" />"
            del style_file

        layout = layout.replace("{stylesheet}", tag_style)
        layout = layout.replace("{javascript}", tag_script)
        del tag_style
        del tag_script
        del self._scripts
        del self._styles

        return layout
