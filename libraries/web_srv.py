from microWebSrv import MicroWebSrv
"""
Get Routes to application
"""
from config.routes import routeHandlers

"""
environment variables to configure start application
"""
from config.environment import welcome_page
from config.environment import templates_path

def srv(hostname):
    mws = MicroWebSrv(routeHandlers=routeHandlers, webPath=templates_path)
    mws.Start(threaded=True)

    # url = "http://" + current_ip + "/welcome.html"
    # url = "http://192.168.1.19/" + welcome_page
    url = "http://" + hostname  + "/" + welcome_page
    print(url)
    mws.SetNotFoundPageUrl(url)
    
