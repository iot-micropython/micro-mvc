"""
Tools is a library with the resources for use in the application
"""
import json
from network import WLAN
from network import STA_IF

from config.environment import get_setting_file
from libraries.tools import if_file_exists 
from libraries.tools import get_setting

def wifi():
    sta = WLAN(STA_IF)
    sta.active(True)
    authmodes_types = ("open", "WEP", "WPA-PSK", "WPA2-PSK", "WPA/WPA2-PSK")
    networks = []
    
    current = False
    data = ""

    if if_file_exists(get_setting_file()):
        data = get_setting(get_setting_file())

    for ssid in sta.scan():
        try:
            print("{} | {}".format(data['ssid'].decode('utf-8'), ssid[0].decode('utf-8')))
            current = data['ssid'].decode('utf-8') == ssid[0].decode('utf-8').replace(" ", "+")
            print("~> ", current)
        except:
            current = False

        networks.append({
            "name" : ssid[0].decode('utf-8'),
            "channel" : ssid[2],
            "auth_mode" : authmodes_types[ssid[4]],
            "selected" : str(current)
        })
    return { 'networks': networks }
