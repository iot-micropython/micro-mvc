import sys
from machine import UART

print("Start application Bar Code Reader")
uart = UART(1, 9600)
uart.init(9600, bits=8, parity=None, stop=1, rx=22,  tx=23)
print(uart.any())
print("End application Bar Code Reader")

while True:
    bar_code = uart.read(21)
    if bar_code is not None:
        print(bar_code)
