import os

class wifi:
    configuration_file = 'json/wifi.json'

    def __init__():
        try:
            os.stat(configuration_file)
            return self.read()
        except:
            return "Error - Wifi configuration file is not created yet."

    def create(ssid, channel, encryption, key):
        print(ssid, channel, encryption, key)
        return True

    def update(ssid, channel, encryption, key):
        self.delete()
        self.created(ssid, channel, encryption, key)

    def read():
        return open(configuration_file)

    def delete():
        os.remove(configuration_file)
